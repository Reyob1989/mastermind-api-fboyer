package org.fboyer.mastermind.repository;

import org.fboyer.mastermind.entity.GameEntity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GameRepositoryTestIT {

	@Autowired
	private GameRepository gameRepository;

	@Test
	public void testCrud() {

		final String userName = "name";
		final String pattern = "pattern";

		GameEntity gameEntity = new GameEntity();
		gameEntity.setCodeMakerName(userName);
		gameEntity.setPattern(pattern);

		//Create
		final GameEntity gameEntitySaved = this.gameRepository.saveAndFlush(gameEntity);

		Assert.assertEquals(userName, gameEntitySaved.getCodeMakerName());
		Assert.assertEquals(pattern, gameEntitySaved.getPattern());

		// Get
		gameEntity = this.gameRepository.findById(gameEntitySaved.getGameId()).get();

		Assert.assertEquals(userName, gameEntitySaved.getCodeMakerName());
		Assert.assertEquals(pattern, gameEntitySaved.getPattern());

		//Update
		final String newUserName = "nameUpdated";

		gameEntity.setCodeMakerName(newUserName);

		final GameEntity gameEntityUpdated = this.gameRepository.saveAndFlush(gameEntity);

		Assert.assertEquals(newUserName, gameEntityUpdated.getCodeMakerName());

		//Delete
		this.gameRepository.delete(gameEntitySaved);
	}

	@Test
	public void GivenValidName_WhenCallFindAllByCodeMakerName_ThenMustReturnResults(){
		final String userName = "name";
		final String pattern = "pattern";

		final GameEntity gameEntity = new GameEntity();
		gameEntity.setCodeMakerName(userName);
		gameEntity.setPattern(pattern);

		//Create
		this.gameRepository.saveAndFlush(gameEntity);

		final List<GameEntity> savedGames = this.gameRepository.findAllByCodeMakerName(userName);

		Assert.assertNotNull(savedGames);
	}

	@Test
	public void GivenValidName_WhenCallFindTop1ByOrderByCreatedAt_ThenMustReturnResults(){
		final String userName = "name";
		final String pattern = "pattern";

		final GameEntity gameEntity = new GameEntity();
		gameEntity.setCodeMakerName(userName);
		gameEntity.setPattern(pattern);

		//Create
		this.gameRepository.saveAndFlush(gameEntity);

		final GameEntity savedGames = this.gameRepository.findTop1ByOrderByCreatedAt();

		Assert.assertNotNull(savedGames);
	}

}