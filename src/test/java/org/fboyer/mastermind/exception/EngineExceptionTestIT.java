package org.fboyer.mastermind.exception;

import org.fboyer.mastermind.model.enumeration.ErrorTypeEnum;
import org.junit.Assert;
import org.junit.Test;

/**
 * Unit Test for {@link EngineException}
 */
public class EngineExceptionTestIT {

    @Test
    public void testEngineExceptionMessage() {
        final String message = "Exception Message";
        final EngineException exception = new EngineException(message);
        Assert.assertEquals(message, exception.getMessage());
        Assert.assertNull(exception.getErrorType());
        Assert.assertNull(exception.getMessageParameters());
    }

    @Test
    public void testEngineExceptionThrowable() {
        final Exception innerException = new Exception();
        final EngineException exception = new EngineException(innerException);

        Assert.assertEquals(innerException, exception.getCause());
        Assert.assertNull(exception.getErrorType());
        Assert.assertNull(exception.getMessageParameters());
    }

    @Test
    public void testEngineExceptionMessageThrowable() {
        final String message = "Exception Message";
        final Exception innerException = new Exception();
        final EngineException exception = new EngineException(message, innerException);

        Assert.assertEquals(message, exception.getMessage());
        Assert.assertEquals(innerException, exception.getCause());
        Assert.assertNull(exception.getErrorType());
        Assert.assertNull(exception.getMessageParameters());
    }

    @Test
    public void testEngineExceptionBaseErrorTypeMessage() {
        final String message = "Exception Message";
        final ErrorTypeEnum errorType = ErrorTypeEnum.SERVER_ERROR;
        final EngineException exception = new EngineException(errorType, message);

        Assert.assertEquals(message, exception.getMessage());
        Assert.assertEquals(errorType, exception.getErrorType());
        Assert.assertNull(exception.getMessageParameters());
    }

    @Test
    public void testEngineExceptionBaseErrorTypeMessageParameters() {
        final String message = "Exception Message";
        final ErrorTypeEnum errorType = ErrorTypeEnum.SERVER_ERROR;
        final Object[] parameters = new Object[]{1, "str"};
        final EngineException exception = new EngineException(errorType, message, parameters);

        Assert.assertEquals(message, exception.getMessage());
        Assert.assertEquals(errorType, exception.getErrorType());
        Assert.assertEquals(parameters[0], exception.getMessageParameters()[0]);
        Assert.assertEquals(parameters[1], exception.getMessageParameters()[1]);
    }

    @Test
    public void testEngineExceptionBaseErrorTypeMessageThrowable() {
        final String message = "Exception Message";
        final Exception innerException = new Exception();
        final ErrorTypeEnum errorType = ErrorTypeEnum.SERVER_ERROR;
        final EngineException exception = new EngineException(errorType, message, innerException);

        Assert.assertEquals(message, exception.getMessage());
        Assert.assertEquals(innerException, exception.getCause());
        Assert.assertEquals(errorType, exception.getErrorType());
        Assert.assertNull(exception.getMessageParameters());
    }

    @Test
    public void testEngineExceptionBaseErrorTypeMessageThrowableParameters() {
        final String message = "Exception Message";
        final Exception innerException = new Exception();
        final ErrorTypeEnum errorType = ErrorTypeEnum.SERVER_ERROR;
        final Object[] parameters = new Object[]{1, "str"};
        final EngineException exception = new EngineException(errorType, message, innerException, parameters);

        Assert.assertEquals(message, exception.getMessage());
        Assert.assertEquals(innerException, exception.getCause());
        Assert.assertEquals(errorType, exception.getErrorType());
        Assert.assertEquals(parameters[0], exception.getMessageParameters()[0]);
        Assert.assertEquals(parameters[1], exception.getMessageParameters()[1]);
    }

    @Test
    public void testEngineExceptionBaseErrorTypeThrowable() {
        final Exception innerException = new Exception("excMsg");
        final ErrorTypeEnum errorType = ErrorTypeEnum.SERVER_ERROR;
        final EngineException exception = new EngineException(errorType, innerException);

        Assert.assertTrue(exception.getMessage().contains("excMsg"));
        Assert.assertEquals(innerException, exception.getCause());
        Assert.assertEquals(errorType, exception.getErrorType());
    }

    @Test
    public void testEngineExceptionBaseErrorTypeThrowableParameters() {
        final Exception innerException = new Exception("excMsg");
        final ErrorTypeEnum errorType = ErrorTypeEnum.SERVER_ERROR;
        final Object[] parameters = new Object[]{1, "str"};
        final EngineException exception = new EngineException(errorType, innerException, parameters);

        Assert.assertTrue(exception.getMessage().contains("excMsg"));
        Assert.assertEquals(innerException, exception.getCause());
        Assert.assertEquals(errorType, exception.getErrorType());
        Assert.assertEquals(parameters[0], exception.getMessageParameters()[0]);
        Assert.assertEquals(parameters[1], exception.getMessageParameters()[1]);
    }

}