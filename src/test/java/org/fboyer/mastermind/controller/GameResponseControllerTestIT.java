package org.fboyer.mastermind.controller;

import org.fboyer.mastermind.model.Game;
import org.fboyer.mastermind.model.GameResponse;
import org.fboyer.mastermind.model.constants.GameResponseUriConstants;
import org.fboyer.mastermind.model.constants.GameUriConstants;
import org.fboyer.mastermind.repository.GameRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URL;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Test class for {@link GameResponseController}
 *
 * @author fboyer on 27/09/2018
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GameResponseControllerTestIT {

	@LocalServerPort
	private int port;

	private URL base;

	@Autowired
	private TestRestTemplate template;

	@Autowired
	private GameRepository gameRepository;

	@Before
	public void setUp() throws Exception {
		this.base = new URL("http://localhost:" + this.port + "/");
		this.gameRepository.deleteAll();
	}

	@Test
	public void processGame() {
		final String codeMakerName = "codeMaker";
		final String pattern = "color,color,color,color";
		final List<String> patternEntry = Arrays.asList("color","color","color","color");
		final Game game = new Game();
		game.setCodeMakerName(codeMakerName);
		game.setPattern(pattern);

		this.template.postForEntity(this.base.toString() + GameUriConstants.GAME, game, Game.class);

		final ResponseEntity<GameResponse> response = this.template.postForEntity(this.base.toString() + GameResponseUriConstants.RESPONSE, patternEntry, GameResponse.class);

		assertThat(response.getBody(), is(notNullValue()));
		assertThat(response.getBody().getBlacks(), is(4));
		assertThat(response.getBody().getOrange(), is(0));
	}

	@Test
	public void processGameDifferentEntry() {
		final String codeMakerName = "name";
		final String pattern = "red,color,color,color";
		final List<String> patternEntry = Arrays.asList("color","red","color","color");

		final Game game = new Game();
		game.setCodeMakerName(codeMakerName);
		game.setPattern(pattern);

		this.template.postForEntity(this.base.toString() + GameUriConstants.GAME, game, Game.class);

		final ResponseEntity<GameResponse> response = this.template.postForEntity(this.base.toString() + GameResponseUriConstants.RESPONSE, patternEntry, GameResponse.class);

		assertThat(response.getBody(), is(notNullValue()));
		assertThat(response.getBody().getBlacks(), is(2));
		assertThat(response.getBody().getOrange(), is(2));
	}

}
