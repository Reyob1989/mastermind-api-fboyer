package org.fboyer.mastermind.controller;

import org.fboyer.mastermind.model.Game;
import org.fboyer.mastermind.model.constants.GameUriConstants;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URL;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Test class for {@link GameController}
 *
 * @author fboyer on 27/09/2018
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GameControllerTestIT {

	@LocalServerPort
	private int port;

	private URL base;

	@Autowired
	private TestRestTemplate template;

	@Before
	public void setUp() throws Exception {
		this.base = new URL("http://localhost:" + this.port + "/");
	}

	@Test
	public void createGame() {
		final String codeMakerName = "codeMaker";
		final String pattern = "color,color,color,color";
		final Game game = new Game();
		game.setCodeMakerName(codeMakerName);
		game.setPattern(pattern);

		final ResponseEntity<Game> response = this.template.postForEntity(this.base.toString() + GameUriConstants.GAME, game,
				Game.class);

		assertThat(response.getBody(), is(notNullValue()));
		assertThat(response.getBody().getGameId(), is(notNullValue()));
		assertThat(response.getBody().getPattern(), is(game.getPattern()));
		assertThat(response.getBody().getCodeMakerName(), is(game.getCodeMakerName()));
	}

	@Test
	public void getAllHistoricGames() {
		final String codeMakerName = "codeMaker";
		final String pattern = "color,color,color,color";
		final Game game = new Game();
		game.setCodeMakerName(codeMakerName);
		game.setPattern(pattern);

		this.template.postForEntity(this.base.toString() + "game", game, Game.class);

		final ResponseEntity<List<Game>> response = this.template.exchange(this.base.toString() + GameUriConstants.HISTORIC, HttpMethod.GET, null, new ParameterizedTypeReference<List<Game>>(){});

		assertThat(response.getBody(), is(notNullValue()));
		assertThat(response.getBody().size(), is(greaterThanOrEqualTo(0)));
	}
}
