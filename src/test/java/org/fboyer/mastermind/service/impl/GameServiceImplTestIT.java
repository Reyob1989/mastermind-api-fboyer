package org.fboyer.mastermind.service.impl;

import org.fboyer.mastermind.entity.GameEntity;
import org.fboyer.mastermind.exception.EngineException;
import org.fboyer.mastermind.model.enumeration.ErrorTypeEnum;
import org.fboyer.mastermind.service.GameService;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static net.bytebuddy.matcher.ElementMatchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GameServiceImplTestIT {

	@Autowired
	private GameService gameService;

	@Test
	public void GivenValidGame_WhenCallCreate_ThenMustCreateGame(){
		final String userName = "name";
		final String pattern = "color,color,color,color";

		final GameEntity gameEntity = new GameEntity();
		gameEntity.setCodeMakerName(userName);
		gameEntity.setPattern(pattern);

		final GameEntity gameEntityCreated = this.gameService.create(gameEntity);

		Assert.assertEquals(userName, gameEntityCreated.getCodeMakerName());
		Assert.assertEquals(pattern, gameEntityCreated.getPattern());
	}

	@Test
	public void GivenInValidGameName_WhenCallCreate_ThenMustThrowErrow(){
		final String userName = "";
		final String pattern = "color,color,color,color";

		final GameEntity gameEntity = new GameEntity();
		gameEntity.setCodeMakerName(userName);
		gameEntity.setPattern(pattern);

		try {
			this.gameService.create(gameEntity);
		} catch (final EngineException e){
			Assert.assertEquals(ErrorTypeEnum.MISSING_PARAMETER,e.getErrorType());
		}
	}

	@Test
	public void GivenInValidGamePattern_WhenCallCreate_ThenMustThrowErrow(){
		final String userName = "name";
		final String pattern = "color,color,color";

		final GameEntity gameEntity = new GameEntity();
		gameEntity.setCodeMakerName(userName);
		gameEntity.setPattern(pattern);

		try {
			this.gameService.create(gameEntity);
		} catch (final EngineException e){
			Assert.assertEquals(ErrorTypeEnum.GAME_NOT_VALID,e.getErrorType());
		}
	}

	@Test
	public void GivenValidGame_WhenCallGet_ThenMustGetCreateGame(){
		final String userName = "name";
		final String pattern = "color,color,color,color";

		final GameEntity gameEntity = new GameEntity();
		gameEntity.setCodeMakerName(userName);
		gameEntity.setPattern(pattern);

		final GameEntity gameSaved =this.gameService.create(gameEntity);

		assertThat(gameSaved.getGameId(), CoreMatchers.is(Matchers.notNullValue()));
		Assert.assertEquals(userName, gameSaved.getCodeMakerName());
		Assert.assertEquals(pattern, gameSaved.getPattern());
	}

	@Test
	public void GivenValidGame_WhenCallGet_ThenMustThrowError(){

		try {
			this.gameService.get(null);
		} catch (final EngineException e){
			Assert.assertEquals(ErrorTypeEnum.MISSING_PARAMETER,e.getErrorType());
		}
	}
}