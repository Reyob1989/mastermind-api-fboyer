package org.fboyer.mastermind.service.impl;

import org.fboyer.mastermind.entity.GameEntity;
import org.fboyer.mastermind.entity.GameResponseEntity;
import org.fboyer.mastermind.exception.EngineException;
import org.fboyer.mastermind.model.enumeration.ColorEnum;
import org.fboyer.mastermind.model.enumeration.ErrorTypeEnum;
import org.fboyer.mastermind.repository.GameRepository;
import org.fboyer.mastermind.service.GameResponseService;
import org.fboyer.mastermind.service.GameService;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.awt.*;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GameResponseServiceImplTestIT {

	@Autowired
	private GameService gameService;

	@Autowired
	private GameResponseService gameResponseService;

	@Autowired
	private GameRepository gameRepository;

	@Before
	public void setUp() throws Exception {
		this.gameRepository.deleteAll();
	}


	@Test
	public void GivenValidGameSamePatternEntry_WhenCallProcess_ThenMustReturnResponse(){
		final String userName = "name";
		final String pattern = "color,color,color,color";
		final List<String> patternEntry = Arrays.asList("color","color","color","color");

		final GameEntity gameEntity = new GameEntity();
		gameEntity.setCodeMakerName(userName);
		gameEntity.setPattern(pattern);

		this.gameService.create(gameEntity);

		final GameResponseEntity gameResponseEntity = this.gameResponseService.process(patternEntry);

		Assert.assertThat(gameResponseEntity.getBlacks(), CoreMatchers.is(4));
		Assert.assertThat(gameResponseEntity.getOrange(), CoreMatchers.is(0));
	}

	@Test
	public void GivenValidGameDifferentColors_WhenCallProcess_ThenMustReturnResponse(){
		final String userName = "name";
		final String pattern = "red,color,color,color";
		final List<String> patternEntry = Arrays.asList("color","red","color","color");

		final GameEntity gameEntity = new GameEntity();
		gameEntity.setCodeMakerName(userName);
		gameEntity.setPattern(pattern);

		this.gameService.create(gameEntity);

		final GameResponseEntity gameResponseEntity = this.gameResponseService.process(patternEntry);

		Assert.assertThat(gameResponseEntity.getBlacks(), CoreMatchers.is(2));
		Assert.assertThat(gameResponseEntity.getOrange(), CoreMatchers.is(2));
	}

	@Test
	public void GivenValidGameDifferentColorsOtherCase_WhenCallProcess_ThenMustCreateGame(){
		final String userName = "name";
		final String pattern = "red,color,color,color";
		final List<String> patternEntry = Arrays.asList("color","black","color","color");

		final GameEntity gameEntity = new GameEntity();
		gameEntity.setCodeMakerName(userName);
		gameEntity.setPattern(pattern);

		this.gameService.create(gameEntity);

		final GameResponseEntity gameResponseEntity = this.gameResponseService.process(patternEntry);

		Assert.assertThat(gameResponseEntity.getBlacks(), CoreMatchers.is(2));
		Assert.assertThat(gameResponseEntity.getOrange(), CoreMatchers.is(1));
	}
}