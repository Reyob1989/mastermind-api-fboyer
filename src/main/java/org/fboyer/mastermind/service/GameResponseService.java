package org.fboyer.mastermind.service;


import org.fboyer.mastermind.entity.GameResponseEntity;

import java.util.List;

/**
 * Game Response Service interface.
 *
 * @author fboyer on 25/09/2018
 */
public interface GameResponseService {

	/**
	 * Game response process for a given entry
	 *
	 * @param patternEntry Entry parameter
	 *
	 * @return GameResponseEntity obtained
	 */
	GameResponseEntity process(final List<String> patternEntry);
}
