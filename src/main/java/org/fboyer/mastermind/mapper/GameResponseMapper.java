package org.fboyer.mastermind.mapper;


import org.fboyer.mastermind.entity.GameResponseEntity;
import org.fboyer.mastermind.model.GameResponse;
import org.mapstruct.Mapper;

/**
 * Mapper for user.
 *
 * @author fboyer on 25/09/2018.
 */
@Mapper(componentModel = "spring")
public interface GameResponseMapper extends MappableEntity<GameResponse, GameResponseEntity>{
}
