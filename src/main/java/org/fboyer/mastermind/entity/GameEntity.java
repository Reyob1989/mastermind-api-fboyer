package org.fboyer.mastermind.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the game database table.
 *
 * @author fboyer on 25/09/2018.
 */
@Entity
public class GameEntity extends AuditableEntity {

	private static final long serialVersionUID = -7759963311207047448L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long gameId;
	private String codeMakerName;
	private String pattern;

	public Long getGameId() {
		return gameId;
	}

	public void setGameId(Long gameId) {
		this.gameId = gameId;
	}

	public String getCodeMakerName() { return codeMakerName; }

	public void setCodeMakerName(String codeMakerName) { this.codeMakerName = codeMakerName; }

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
}