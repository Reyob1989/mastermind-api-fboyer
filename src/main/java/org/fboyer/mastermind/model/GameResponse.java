package org.fboyer.mastermind.model;

/**
 * The persistent class for the game database table.
 *
 * @author fboyer on 25/09/2018.
 */
public class GameResponse {


	private Integer blacks;
	private Integer orange;

	public Integer getBlacks() { return blacks; }

	public void setBlacks(Integer blacks) { this.blacks = blacks; }

	public Integer getOrange() { return orange; }

	public void setOrange(Integer orange) { this.orange = orange; }
}