package org.fboyer.mastermind.model.constants;

/**
 * Countries REST Service URI Constants
 *
 * @author fboyer on 25/09/2018.
 */
@SuppressWarnings("PMD.AvoidConstantsInterface")
public interface GameResponseUriConstants {

	String RESPONSE = "response";
}
